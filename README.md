[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kitware.com%2Fcmaurini%2Fnewfrac-core-numerics.git/HEAD)

# Basic computational methods for fracture mechanics

This repository contains the material for the 4-hour lectures on the basics of computational methods for fracture (21/01/2021), as taught during the NEWFRAC Core School 2021.

**The project moved to https://gitlab.com/newfrac/CORE-school/newfrac-core-numerics**

## Acknowledgements

The funding received from the European Union’s Horizon 2020 research and innovation programme under Marie Skłodowska-Curie grant agreement No. 861061-NEWFRAC is gratefully acknowledged.
